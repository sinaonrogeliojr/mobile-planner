CREATE TABLE IF NOT EXISTS tbl_schedules(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    activity TEXT, 
    date_added INTEGER,
    time_added INTEGER
);

INSERT or IGNORE INTO tbl_schedules(id, activity, date_added,time_added) VALUES (1, 'Coding', '1288323623006', '1288323623006');
