// app-routing.module.ts

import { NgModule } from "@angular/core";
import { PreloadAllModules, RouterModule, Routes } from "@angular/router";

const routes: Routes = [
  // { path: "", redirectTo: "home", pathMatch: "full" },
  {
    path: "home",
    loadChildren: () =>
      import("./home/home.module").then((m) => m.HomePageModule),
  },
  {
    path: "schedule/:id",
    loadChildren: () =>
      import("./schedule/schedule.module").then((m) => m.SchedulePageModule),
  },
  {
    path: "",
    redirectTo: "site",
    pathMatch: "full",
  },
  {
    path: "site",
    loadChildren: () => import("./site/site.module").then((m) => m.SiteModule),
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
