import { NgModule } from "@angular/core";
import { SharedModule } from "../shared/shared.module";
import { ForgotPasswordComponent } from "./forgot-password/forgot-password.component";
import { SigninComponent } from "./signin/signin.component";
import { SignupComponent } from "./signup/signup.component";
import { SiteRoutingModule } from "./site.module.routing";
import { SiteComponent } from "./site/site.component";

@NgModule({
  declarations: [
    SigninComponent,
    SiteComponent,
    SignupComponent,
    ForgotPasswordComponent,
  ],
  imports: [SiteRoutingModule, SharedModule],
})
export class SiteModule {}
