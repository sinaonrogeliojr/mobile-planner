import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup } from "@angular/forms";

@Component({
  selector: "app-signup",
  templateUrl: "./signup.component.html",
  styleUrls: ["./signup.component.scss"],
})
export class SignupComponent implements OnInit {
  constructor(private fb: FormBuilder) {}

  ngOnInit() {}
  form: FormGroup = this.fb.group({
    username: null,
    password: null,
    name: null,
  });
}
