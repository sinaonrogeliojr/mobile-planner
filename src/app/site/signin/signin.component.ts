import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup } from "@angular/forms";
import { Router } from "@angular/router";

@Component({
  selector: "app-signin",
  templateUrl: "./signin.component.html",
  styleUrls: ["./signin.component.scss"],
})
export class SigninComponent implements OnInit {
  constructor(private fb: FormBuilder, private router: Router) {}
  form: FormGroup = this.fb.group({
    username: null,
    password: null,
  });
  ngOnInit() {}
  login() {
    this.router.navigate(["home"]);
  }
}
