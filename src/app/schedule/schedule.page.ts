import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from "@angular/forms";
import { DbService } from './../services/db.service'
import { ActivatedRoute, Router } from "@angular/router";

@Component({
  selector: 'app-schedule',
  templateUrl: './schedule.page.html',
  styleUrls: ['./schedule.page.scss'],
})
export class SchedulePage implements OnInit {
  editForm: FormGroup;
  id: any;

  constructor(
    private db: DbService,
    private router: Router,
    public formBuilder: FormBuilder,
    private actRoute: ActivatedRoute
  ) {
    this.id = this.actRoute.snapshot.paramMap.get('id');

    this.db.getSchedule(this.id).then(res => {
      // alert(this.id);
      this.editForm.setValue({
        activity: res['activity'],
        date_added: res['date_added'],
        time_added: res['time_added']
      })
    })
  }

  ngOnInit() {
    this.editForm = this.formBuilder.group({
      activity: [''],
      date_added: [''],
      time_added: ['']
    })
  }

  saveForm(){
    this.db.updateSchedule(this.id, this.editForm.value)
    .then( (res) => {
      console.log(res)
      this.router.navigate(['/home']);
    })
  }

}