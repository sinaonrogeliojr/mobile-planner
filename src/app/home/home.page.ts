//home.page.ts

import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from "@angular/forms";
import { DbService } from './../services/db.service';
import { ToastController } from '@ionic/angular';
import { Router } from "@angular/router";

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})

export class HomePage implements OnInit {
  mainForm: FormGroup;
  Data: any[] = []

  constructor(
    private db: DbService,
    public formBuilder: FormBuilder,
    private toast: ToastController,
    private router: Router
  ) { }

  ngOnInit() {
    this.db.dbState().subscribe((res) => {
      if(res){
        this.db.fetchSchedules().subscribe(item => {
          this.Data = item
        })
      }
    });

    this.mainForm = this.formBuilder.group({
      activity: [''],
      date_added: [''],
      time_added: ['']
    })
  }

  storeData() {
    this.db.addSchedule(
      this.mainForm.value.activity,
      this.mainForm.value.date_added,
      this.mainForm.value.time_added
    ).then((res) => {
      this.mainForm.reset();
    })
  }

  deleteSchedule(id){
    this.db.deleteSchedule(id).then(async(res) => {
      let toast = await this.toast.create({
        message: 'Schedule deleted',
        duration: 2500
      });
      toast.present();      
    })
  }
  
  

}