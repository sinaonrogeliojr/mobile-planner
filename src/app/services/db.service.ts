// db.service.ts

import { Injectable } from '@angular/core';
import { Platform } from '@ionic/angular';
import { Schedule } from './schedule';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { SQLitePorter } from '@ionic-native/sqlite-porter/ngx';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite/ngx';

@Injectable({
  providedIn: 'root'
})

export class DbService {
  private storage: SQLiteObject;
  schedulesList = new BehaviorSubject([]);
  private isDbReady: BehaviorSubject<boolean> = new BehaviorSubject(false);

  constructor(
    private platform: Platform, 
    private sqlite: SQLite, 
    private httpClient: HttpClient,
    private sqlPorter: SQLitePorter,
  ) {
    this.platform.ready().then(() => {
      this.sqlite.create({
        name: 'mp.db',
        location: 'default'
      })
      .then((db: SQLiteObject) => {
          this.storage = db;
          this.getFakeData();
      });
    });
  }

  dbState() {
    return this.isDbReady.asObservable();
  }
 
  fetchSchedules(): Observable<Schedule[]> {
    return this.schedulesList.asObservable();
  }

    // Render fake data
    getFakeData() {
      this.httpClient.get(
        'assets/dump.sql', 
        {responseType: 'text'}
      ).subscribe(data => {
        this.sqlPorter.importSqlToDb(this.storage, data)
          .then(_ => {
            this.getSchedules();
            this.isDbReady.next(true);
          })
          .catch(error => console.error(error));
      });
    }

  // Get list
  getSchedules(){
    return this.storage.executeSql('SELECT * FROM tbl_schedules', []).then(res => {
      let items: Schedule[] = [];
      if (res.rows.length > 0) {
        for (var i = 0; i < res.rows.length; i++) { 
          items.push({ 
            id: res.rows.item(i).id,
            activity: res.rows.item(i).activity,
            date_added: res.rows.item(i).date_added,  
            time_added: res.rows.item(i).time_added
           });
        }
      }
      this.schedulesList.next(items);
    });
  }

  // Add
  addSchedule(activity, date_added,time_added) {
    let data = [activity, date_added, time_added];
    return this.storage.executeSql('INSERT INTO tbl_schedules (activity, date_added,time_added) VALUES (?, ?, ?)', data)
    .then(res => {
      this.getSchedules();
    });
  }
 
  // Get single object
  getSchedule(id): Promise<Schedule> {
    return this.storage.executeSql('SELECT * FROM tbl_schedules WHERE id = ?', [id]).then(res => { 
      return {
        id: res.rows.item(0).id,
        activity: res.rows.item(0).activity, 
        date_added: res.rows.item(0).date_added,  
        time_added: res.rows.item(0).time_added
      }
    });
  }

  // Update
  updateSchedule(id, sched: Schedule) {
    let data = [sched.activity, sched.date_added, sched.time_added];
    return this.storage.executeSql(`UPDATE tbl_schedules SET activity = ?, date_added = ?, time_added = ? WHERE id = ${id}`, data)
    .then(data => {
      this.getSchedules();
    })
  }

  // Delete
  deleteSchedule(id) {
    return this.storage.executeSql('DELETE FROM tbl_schedules WHERE id = ?', [id])
    .then(_ => {
      this.getSchedules();
    });
  }
}